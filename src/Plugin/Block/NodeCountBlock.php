<?php

namespace Drupal\node_count\Plugin\Block;

use Drupal\Core\Database\Database;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a "Node count block".
 *
 * @Block(
 *  id = "node_count",
 *  admin_label = @Translation("Node count"),
 * )
 */
class NodeCountBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#theme' => 'block--node-count',
      '#desc' => 'Description here',
      '#data' => $this->nodeCount(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function nodeCount() {

    $connection = Database::getConnection();
    $query = $connection->select('node_field_data', 'n');
    $query->fields('n', ['type', 'status']);
    $query->addExpression('COUNT(type)', 'count');
    $query->groupBy("n.type");
    $query->groupBy("n.status");
    $query->orderBy('n.type', 'ASC');
    $query->orderBy('n.status', 'DESC');
    $node_count = $query->execute()->fetchAll();
    foreach ($node_count as $value) {
      $return[$value->type][$value->status]['count'] = $value->count;
      $return[$value->type][$value->status]['status'] = ($value->status == 1) ? 'Publish' : 'Unpublish';
    }

    return $return;
  }

}
