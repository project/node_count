Node count 8.x-1.x
--------------------------
INTRODUCTION
------------
This module shows the published and unpublished node count of each content type.


INSTALLATION
------------
* Enable the module
* Go to url admin > modules (admin/modules)

REQUIREMENTS
------------
Show the published and unpublished node count.

CONFIGURATION
-------------
* Go to url admin > structure > block (admin/structure/block)
* Node count block assign in your regions.

Author
------
Vishal Sirsodiya
